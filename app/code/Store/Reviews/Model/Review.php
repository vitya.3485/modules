<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Model;

use Magento\Framework\Model\AbstractModel;
use Store\Reviews\Api\Data\ReviewInterface;
use Store\Reviews\Model\ResourceModel\Review as Resource;

/**
 * Review model
 */
class Review extends AbstractModel implements ReviewInterface
{
    /**
     * Model initialization
     *
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init(Resource::class);
    }

    /**
     * {@inheirtdoc}
     */
    public function setReviewId(int $id): void
    {
        $this->setData(self::REVIEW_ID, $id);
    }

    /**
     * {@inheirtdoc}
     */
    public function getReviewId(): int
    {
        return (int) $this->getData(self::REVIEW_ID);
    }

    /**
     * {@inheirtdoc}
     */
    public function setCustomerId(int $id): void
    {
        $this->setData(self::CUSTOMER_ID, $id);
    }

    /**
     * {@inheirtdoc}
     */
    public function getCustomerId(): int
    {
        return (int) $this->getData(self::CUSTOMER_ID);
    }

    /**
     * {@inheirtdoc}
     */
    public function setCustomerName(string $customerName): void
    {
        $this->setData(self::CUSTOMER_NAME, $customerName);
    }

    /**
     * {@inheirtdoc}
     */
    public function getCustomerName(): string
    {
        return $this->getData(self::CUSTOMER_NAME) ?? '';
    }

    /**
     * {@inheirtdoc}
     */
    public function setTitle(string $title): void
    {
        $this->setData(self::TITLE, $title);
    }

    /**
     * {@inheirtdoc}
     */
    public function getTitle(): string
    {
        return $this->getData(self::TITLE) ?? '';
    }

    /**
     * {@inheirtdoc}
     */
    public function setReview(string $review): void
    {
        $this->setData(self::REVIEW, $review);
    }

    /**
     * {@inheirtdoc}
     */
    public function getReview(): string
    {
        return $this->getData(self::REVIEW) ?? '';
    }

    /**
     * {@inheirtdoc}
     */
    public function setPros(string $pros): void
    {
        $this->setData(self::PROS, $pros);
    }

    /**
     * {@inheirtdoc}
     */
    public function getPros(): string
    {
        return $this->getData(self::PROS) ?? '';
    }

    /**
     * {@inheirtdoc}
     */
    public function setCons(string $cons): void
    {
        $this->setData(self::CONS, $cons);
    }

    /**
     * {@inheirtdoc}
     */
    public function getCons(): string
    {
        return $this->getData(self::CONS) ?? '';
    }
}
