<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Model;

use Magento\Customer\Model\Session;
use Store\Reviews\Api\Data\ReviewInterface;
use Store\Reviews\Api\ReviewsApiInterface;
use Store\Reviews\Model\ResourceModel\Review;
use Store\Reviews\Model\ResourceModel\Review\Collection;
use Store\Reviews\Model\Review as ModelReview;
use Exception;

/**
 * Reviews Rest API
 *
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class ReviewsApi implements ReviewsApiInterface
{
    /**
     * Constructor
     *
     * @param Review      $resource
     * @param ModelReview $model
     * @param Collection  $collection
     * @param Session     $session
     */
    public function __construct(
        private readonly Review      $resource,
        private readonly ModelReview $model,
        private readonly Collection  $collection,
        private readonly Session     $session
    ) {
    }

    /**
     * Get item by id
     *
     * @param string $id
     * @return array
     */
    public function get(string $id): array
    {
        if (!empty($id)) {
            $model = $this->model;
            $this->resource->load($model, $id);
            return [$model->getData()];
        } else {
            return ["error" => (string)__("Something went wrong")];
        }
    }

    /**
     * Get all items
     *
     * @return array
     */
    public function getList(): array
    {
        return $this->collection->getData();
    }

    /**
     * Save review
     *
     * @param string $title
     * @param string $review
     * @param string[] $pros
     * @param string[] $cons
     * @return array
     */
    public function save(string $title, string $review, array $pros, array $cons): array
    {
        /** @var ReviewInterface $model */
        $model      = $this->model;
        $resource   = $this->resource;
        $closure    = fn($item) => !empty(trim($item));
        $resultPros = array_filter($pros, $closure) ?? [];
        $resultCons = array_filter($cons, $closure) ?? [];
        $customer   = $this->session->getCustomer();

        try {
            $model->setCustomerId((int)$customer->getId());
            $model->setCustomerName($customer->getName());
            $model->setTitle($title);
            $model->setReview($review);
            $model->setPros(json_encode($resultPros));
            $model->setCons(json_encode($resultCons));
            $resource->save($model);
        } catch (Exception) {
            return ["error" => (string)__("Something went wrong")];
        }

        return ["success" => "ok"];
    }
}
