<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Store\Reviews\Api\Data\ReviewInterface;

/**
 * Review resource model
 */
class Review extends AbstractDb
{
    public const MAIN_TABLE = 'store_review';

    /**
     * Model initialization
     *
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init(self::MAIN_TABLE, ReviewInterface::REVIEW_ID);
    }
}
