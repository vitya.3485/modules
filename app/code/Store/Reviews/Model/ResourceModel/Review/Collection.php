<?php

declare(strict_types=1);

namespace Store\Reviews\Model\ResourceModel\Review;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Store\Reviews\Model\Review;
use Store\Reviews\Model\ResourceModel\Review as Resource;

/**
 * Review collection
 */
class Collection extends AbstractCollection
{
    /**
     * Collection initialization
     *
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init(Review::class, Resource::class);
    }
}
