<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Test\Unit\Block\Widget;

use Store\Reviews\Block\Widget\Reviews;
use Store\Reviews\Model\ResourceModel\Review\CollectionFactory;
use Magento\Framework\View\Element\Template\Context;
use PHPUnit\Framework\TestCase;

/**
 * Reviews widget test
 */
class ReviewsTest extends TestCase
{
    /**
     * Testing object
     *
     * @var Reviews
     */
    protected Reviews $block;

    /**
     * Set up test
     *
     * @return void
     */
    public function setUp(): void
    {
        $context = $this->createMock(Context::class);
        $factory = $this->createMock(CollectionFactory::class);

        $this->block = new Reviews($factory, $context, []);
    }

    /**
     * Test rendering function
     *
     * @dataProvider checkRenderingDataProvider
     */
    public function testCheckRendering(string $arg, bool $res): void
    {
        $result = $this->block->checkRendering($arg);
        $this->assertIsBool($result);
        $this->assertSame($result, $res);
    }

    /**
     * Check function rendering
     *
     * @dataProvider toDataArrayDataProvider
     */
    public function testToDataArray(string $case, array $contains): void
    {
        $result = $this->block->toDataArray($case);
        $this->assertIsString($result);
        $this->assertStringStartsWith('<ul', $result);
        $this->assertStringEndsWith('</ul>', $result);

        foreach ($contains as $contain) {
            $this->assertStringContainsString($contain, $result);
        }
    }

    /**
     * JSON cases
     *
     * @return array[]
     */
    public function toDataArrayDataProvider(): array
    {
        return [
            ['', []],
            ['[]', []],
            ['["first", "second"]', ['<li>first</li>', '<li>second</li>']]
        ];
    }

    /**
     * Test cases for checkRendering functions
     *
     * @return array[]
     */
    public function checkRenderingDataProvider(): array
    {
        return [
            [
                'arg'    => '',
                'result' => false,
            ],
            [
                'arg'    => '[]',
                'result' => false,
            ],
            [
                'arg'    => 'not-empty',
                'result' => true,
            ],
        ];
    }
}
