<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Test\Unit\Ui\DataProvider\Review\Listing;

use Store\Reviews\Ui\DataProvider\Review\Listing\ConsColumn;
use Store\Reviews\Ui\DataProvider\Review\Listing\ProsColumn;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use PHPUnit\Framework\TestCase;

/**
 * Reviews widget
 */
class ColumnTest extends TestCase
{
    /**
     * Cons column class for testing
     *
     * @var ConsColumn
     */
    protected ConsColumn $consColumn;

    /**
     * Pros column class for testing
     *
     * @var ProsColumn
     */
    protected ProsColumn $prosColumn;

    /**
     * Set up test
     *
     * @return void
     */
    public function setUp(): void
    {
        $context = $this->createMock(ContextInterface::class);
        $factory = $this->createMock(UiComponentFactory::class);

        $this->consColumn = new ConsColumn($context, $factory, [], []);
        $this->prosColumn = new ProsColumn($context, $factory, [], []);
    }

    /**
     * Prepare data source test
     *
     * @dataProvider prepareDataSourceDataProvider
     */
    public function testPrepareDataSource(array $value): void
    {
        $consResult = $this->consColumn->prepareDataSource($value);
        $prosResult = $this->prosColumn->prepareDataSource($value);

        foreach ($consResult['data']['items'] as $item) {
            $this->assertArrayHasKey(ConsColumn::COLUMN, $item);
            $this->assertSame($item[ConsColumn::COLUMN], $item['result']);
        }

        foreach ($prosResult['data']['items'] as $item) {
            $this->assertArrayHasKey(ProsColumn::COLUMN, $item);
            $this->assertSame($item[ProsColumn::COLUMN], $item['result']);
        }
    }

    /**
     * Test case for prepare data source function
     *
     * @return array
     */
    public function prepareDataSourceDataProvider(): array
    {
        return [
            [
                [
                    'data' => [
                        'items' => [
                            [
                                ConsColumn::COLUMN => '[]',
                                ProsColumn::COLUMN => '[]',
                                'result' => ''
                            ],
                            [
                                ConsColumn::COLUMN => '',
                                ProsColumn::COLUMN => '',
                                'result' => ''
                            ],
                            [
                                ConsColumn::COLUMN => '["cons2"]',
                                ProsColumn::COLUMN => '["cons2"]',
                                'result' => '<div>- cons2</div>'
                            ],
                        ]
                    ]
                ]
            ]
        ];
    }
}
