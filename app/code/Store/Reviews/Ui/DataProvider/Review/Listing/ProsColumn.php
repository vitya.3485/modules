<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Ui\DataProvider\Review\Listing;

use Store\Reviews\Api\Data\ReviewInterface;

/**
 * Pros column renderer
 */
class ProsColumn extends Column
{
    public const COLUMN = ReviewInterface::PROS;
}
