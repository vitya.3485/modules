<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Ui\DataProvider\Review\Listing;

use Store\Reviews\Api\Data\ReviewInterface;

/**
 * Cons column renderer
 */
class ConsColumn extends Column
{
    public const COLUMN = ReviewInterface::CONS;
}
