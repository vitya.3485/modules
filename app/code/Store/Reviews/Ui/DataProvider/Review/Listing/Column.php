<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Ui\DataProvider\Review\Listing;

use Magento\Ui\Component\Listing\Columns\Column as ParentColumn;

/**
 * Parent column renderer
 */
class Column extends ParentColumn
{
    /**
     * Column to render
     */
    public const COLUMN = '';

    /**
     * Change two fields
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[static::COLUMN] = $this->getHtmlResult($item[static::COLUMN] ?? '');
            }
        }

        return $dataSource;
    }

    /**
     * Format result as list
     *
     * @param string $column
     * @return string
     */
    protected function getHtmlResult(string $column): string
    {
        $result = '';
        $columnForJson = json_decode($column);
        if (!empty($columnForJson)) {
            foreach ($columnForJson as $item) {
                $result .= '<div>- ' . $item . '</div>';
            }
        } else {
            $result .= $column;
        }

        if ($result == '[]') {
            $result = '';
        }

        return $result;
    }
}
