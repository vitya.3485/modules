<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Store\Reviews\Api\Data\ReviewInterface;
use Store\Reviews\Model\ResourceModel\Review\CollectionFactory;

/**
 * Reviews widget
 */
class Reviews extends Template
{
    /**
     * Widget template
     *
     * @var string
     */
    protected $_template = 'widget/review.phtml';

    /**
     * Constructor
     *
     * @param CollectionFactory    $collection
     * @param Context              $context
     * @param array                $data
     */
    public function __construct(
        private readonly CollectionFactory    $collection,
        Context                               $context,
        array                                 $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Get all reviews
     *
     * @return ReviewInterface[]|null
     */
    public function getReviews(): ?array
    {
        return $this->collection->create()->getItems();
    }

    /**
     * Render pros and cons
     *
     * @param string $column
     * @return string
     */
    public function toDataArray(string $column): string
    {
        $result = '<ul>';
        $arr    = json_decode($column);

        if (!empty($arr)) {
            foreach ($arr as $item) {
                $result .= '<li>' . $item . '</li>';
            }
        }
        $result .= '</ul>';
        return $result;
    }

    /**
     * Check if pros or cons will be rendered
     *
     * @param string $column
     * @return bool
     */
    public function checkRendering(string $column): bool
    {
        return $column != '[]' && !empty($column);
    }
}
