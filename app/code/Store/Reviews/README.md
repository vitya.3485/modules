<h1 align="center">Store Review module</h1>

This module have functionality to add reviews to store. Reviews can add only logged in customer in **My Account > Store Reviews**.

![customer-tabs](DocImages/customer-tabs.png)

**Form to add reviews**

![form](DocImages/form.png)

**Successfully added review**

![success-form](DocImages/success-form.png)

**Where is the grid in admin menu? STORE REVIEW > Store review > Store review grid**

![admin-menu](DocImages/admin-menu.png)

**Grid have support Mass Delete**

![admin-grid](DocImages/admin-grid.png)

**Reviews can be shown in widgets tab CONTENT > Widget > Add Widget**

![admin-widget](DocImages/admin-widget.png)

**View from customer side**

![customer-reviews](DocImages/customer-reviews.png)

**ACL**

Module have custom acl: Store_Reviews::store_review

## Module have custom REST API

#### GET /rest/V1/reviews

```angular2html
[
    {
    "id": "2",
    "customer_id": "1",
    "customer_name": "test test",
    "title": "testing",
    "review": "testing testing ertyuio xcvl dcdsc dcsdc ds",
    "pros": "[\"sss\"]",
    "cons": "[\"sss\",\"ddd\",\"ggg\"]"
    },
    {
    "id": "3",
    "customer_id": "1",
    "customer_name": "test test",
    "title": "sfdhlrbeljbfwe ",
    "review": "ewjblwjhc ehw jerfew cew cer",
    "pros": "[\"dkjcbkjsdcc wdcwe\",\"ewce\"]",
    "cons": "[]"
    },
]
```

#### GET /rest/V1/reviews/2

```angular2html
[
    {
        "id": "2",
        "customer_id": "1",
        "customer_name": "test test",
        "title": "testing",
        "review": "testing testing ertyuio xcvl dcdsc dcsdc ds",
        "pros": "[\"sss\"]",
        "cons": "[\"sss\",\"ddd\",\"ggg\"]"
    }
]
```

#### POST /rest/V1/reviews/save (only for logged in customers)

![save-request](DocImages/save-request.png)

### ToDo

- Add pagination 
- Add page size and pages to API
- Add GraphQl support
- Add GraphQl Cache
- Add GraphQl Cache invalidation
- Remove customer name from table and use joins and cascade delete instead or remove customer id
- Add statuses to show only specific reviews

### Code style commands

```angular2html
PHPMD
vendor/bin/phpmd app/code/Store/Reviews text dev/tests/static/testsuite/Magento/Test/Php/_files/phpmd/ruleset.xml

PHPCS
vendor/bin/phpcs --colors --standard=Magento2,PSR12 app/code/Store/Reviews

PHPUNIT - Unit tests
vendor/bin/phpunit /home/vitya/magento/ddev-magento2/app/code/Store/Reviews/Test/Unit
```
