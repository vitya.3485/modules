<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Controller\Form;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;

/**
 * Form controller
 */
class Index implements HttpGetActionInterface
{
    /**
     * Constructor
     *
     * @param Session       $session
     * @param ResultFactory $resultFactory
     */
    public function __construct(
        private readonly Session       $session,
        private readonly ResultFactory $resultFactory,
    ) {
    }

    /**
     * This controller responsible for customer form
     *
     * @return Page|Redirect
     */
    public function execute(): Page|Redirect
    {
        if (!$this->session->isLoggedIn()) {
            /** @var Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('/');
        }
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
