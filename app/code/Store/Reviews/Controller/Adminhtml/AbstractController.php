<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Controller\Adminhtml;

use Magento\Backend\App\Action;

/**
 * Parent controller for all admin controllers
 */
abstract class AbstractController extends Action
{
    /**
     * Allowed resource
     */
    public const ADMIN_RESOURCE = 'Store_Reviews::store_review';
}
