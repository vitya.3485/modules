<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Controller\Adminhtml\Grid;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Store\Reviews\Controller\Adminhtml\AbstractController;

/**
 * Controller for grid
 */
class Index extends AbstractController implements HttpGetActionInterface
{
    /**
     * Constructor
     *
     * @param PageFactory $pageFactory
     * @param Context     $context
     */
    public function __construct(
        private readonly PageFactory $pageFactory,
        Context                      $context
    ) {
        parent::__construct($context);
    }

    /**
     * This controller responsible for grid
     *
     * @return Page
     */
    public function execute(): Page
    {
        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu('Store_Reviews::grid');
        $resultPage->getConfig()->getTitle()->prepend(__('Store reviews'));
        return $resultPage;
    }
}
