<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Controller\Adminhtml\Grid;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NotFoundException;
use Magento\Ui\Component\MassAction\Filter;
use Store\Reviews\Controller\Adminhtml\AbstractController;
use Store\Reviews\Model\ResourceModel\Review;
use Store\Reviews\Model\Review as ModelReviewFactory;
use Store\Reviews\Model\ResourceModel\Review\CollectionFactory;
use Psr\Log\LoggerInterface;
use Exception;

/**
 * Controller for mass delete
 */
class MassDelete extends AbstractController implements HttpPostActionInterface
{
    /**
     * Constructor
     *
     * @param Filter             $filter
     * @param ModelReviewFactory $reviewModel
     * @param CollectionFactory  $collection
     * @param Review             $resource
     * @param LoggerInterface    $logger
     * @param Context            $context
     */
    public function __construct(
        private readonly Filter             $filter,
        private readonly ModelReviewFactory $reviewModel,
        private readonly CollectionFactory  $collection,
        private readonly Review             $resource,
        private readonly LoggerInterface    $logger,
        Context                             $context,
    ) {
        parent::__construct($context);
    }

    /**
     * Mass delete logic
     *
     * @return Redirect
     * @throws NotFoundException
     */
    public function execute(): Redirect
    {
        if (!$this->getRequest()->isPost()) {
            throw new NotFoundException(__('Page not found'));
        }

        $reviewDelete = 0;

        try {
            $collection = $this->filter->getCollection($this->collection->create());
            foreach ($collection->getItems() as $review) {
                $this->resource->delete($review);
                $reviewDelete++;
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
        }

        if ($reviewDelete !== 0) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $reviewDelete)
            );
        }

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('review/grid/index');
    }
}
