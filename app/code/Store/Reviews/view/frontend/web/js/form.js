/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */
define([
    'uiComponent',
    'jquery',
    'mage/url',
    'mage/validation',
    ],
    function(Component, $, urlBuilder, messages) {
        let extend = Component.extend({
            defaults: {
                pros: 0,
                cons: 0
            },

            addCons: function () {
                if (this.cons < 4) {
                    ++this.cons;
                    let input = $('<input>', {'name':'cons[' + this.cons + ']', 'class':'margin-top-button', 'type':'text'});
                    input.insertBefore('#cons-but');
                } else {
                    $('#cons-but').css('display', 'none');
                }
            },

            addPros: function () {
                if (this.pros < 4) {
                    ++this.pros;
                    let input = $('<input>', {'name':'pros[' + this.pros + ']', 'class':'margin-top-button', 'type':'text'});
                    input.insertBefore('#pros-but');
                } else {
                    $('#pros-but').css('display', 'none');
                }
            },

            submitForm: function (element) {
                if($(element).validation() && $(element).validation('isValid')) {
                    $('body').trigger('processStart');
                    let formData = new FormData(element);
                    $.ajax({
                        url: urlBuilder.build('rest/V1/reviews/save'),
                        type: "POST",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: formData,
                        showLoader: true,
                        success: function(data) {
                            if (data[0] == 'ok') {
                                $(element).css('display', 'none');
                                $('#success-review').css('display', 'block');
                            }
                        },
                    });
                    $('body').trigger('processStop');
                }
                return false;
            }
        });
        return extend;
    }
);
