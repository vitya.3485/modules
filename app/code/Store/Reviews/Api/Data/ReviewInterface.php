<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Api\Data;

/**
 * Table column constants and getters and setters
 */
interface ReviewInterface
{
    /**
     * Table constants
     */
    public const REVIEW_ID = 'id';
    public const CUSTOMER_ID = 'customer_id';
    public const CUSTOMER_NAME = 'customer_name';
    public const TITLE = 'title';
    public const REVIEW = 'review';
    public const PROS = 'pros';
    public const CONS = 'cons';

    /**
     * @param int $id
     * @return void
     */
    public function setReviewId(int $id): void;

    /**
     * @return int
     */
    public function getReviewId(): int;

    /**
     * @param int $id
     * @return void
     */
    public function setCustomerId(int $id): void;

    /**
     * @return int
     */
    public function getCustomerId(): int;

    /**
     * @param string $customerName
     * @return void
     */
    public function setCustomerName(string $customerName): void;

    /**
     * @return string
     */
    public function getCustomerName(): string;

    /**
     * @param string $title
     * @return void
     */
    public function setTitle(string $title): void;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @param string $review
     * @return void
     */
    public function setReview(string $review): void;

    /**
     * @return string
     */
    public function getReview(): string;

    /**
     * @param string $pros
     * @return void
     */
    public function setPros(string $pros): void;

    /**
     * @return string
     */
    public function getPros(): string;

    /**
     * @param string $cons
     * @return void
     */
    public function setCons(string $cons): void;

    /**
     * @return string
     */
    public function getCons(): string;
}
