<?php

/**
 * @author Victor Andrushkiv vitya.3485@gmail.com
 */

declare(strict_types=1);

namespace Store\Reviews\Api;

/**
 * Interface for rest api
 */
interface ReviewsApiInterface
{
    /**
     * Save review
     *
     * @param string $title
     * @param string $review
     * @param string[] $pros
     * @param string[] $cons
     * @return array
     */
    public function save(string $title, string $review, array $pros, array $cons): array;

    /**
     * Get review by id
     *
     * @param string $id
     * @return array
     */
    public function get(string $id): array;

    /**
     * Get all reviews
     *
     * @return array
     */
    public function getList(): array;
}
